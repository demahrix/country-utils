
class CountryModel {
  
  final String name;
  final String isoCode;
  final String longIsoCode;
  final List<int> code;
  final String imageName;
  
  const CountryModel._(
    this.isoCode,
    this.longIsoCode,
    this.code,
    this.name,
    this.imageName
  );
  
  @override
  int get hashCode => isoCode.hashCode;

  @override
  bool operator ==(Object other) {
    if (!(other is CountryModel))
      return false;

    return identical(this, other) || isoCode == other.isoCode;
  }

  /// Recherche un pays en donnant son code iso \
  /// exemple: `BF` ou `BFA` pour le burkina Faso
  static CountryModel? findByIsoCode(String code) {
    for (final c in values)
      if (c.isoCode == code || c.longIsoCode == code)
        return c;
    return null;
  }

  static CountryModel? findByCode(int code) {
    for (final c in values)
      if (c.code.indexOf(code) != -1)
        return c;
    return null;
  }

  List<CountryModel> search(String value) {
    throw UnimplementedError();
  }

  static List<CountryModel> get values => const [
    AF,
    ZA,
    DZ,
    DE,
    AD,
    AO,
    AG,
    SA,
    AR,
    AM,
    AU,
    AT,
    AZ,

    BS,
    BH,
    BD,
    BB,
    BE,
    BZ,
    BJ,
    BT,
    BY,
    BO,
    BA,
    BW,
    BR,
    BN,
    BG,
    BF,
    BI,

    CI,
    GA
  ];

  static const AF = CountryModel._("AF", "AFG", [93], "Afghanistan", "assets/flags/AF.png");
  static const ZA = CountryModel._("ZA", "ZAF", [-1], "Afrique du Sud", "assets/flags/ZA.png");
  static const AL = CountryModel._("AL", "ALB", [355], "Albanie", "assets/flags/AL.png");
  static const DZ = CountryModel._("DZ", "DZA", [213], "Algérie", "assets/flags/DZ.png");
  static const DE = CountryModel._("DE", "DEU", [-1], "Allemagne", "assets/flags/DE.png");
  static const AD = CountryModel._("AD", "AND", [376], "Andorre", "assets/flags/AD.png");
  static const AO = CountryModel._("AO", "AGO", [244], "Angola", "assets/flags/AO.png"); // FIXME: pas de drapeau
  // static const AI = CountryModel._("AI", "AIQ", [244], "Anguilla", "assets/flags/AI.png");
  static const AG = CountryModel._("AG", "ATG", [1268], "Antigua-et-Barbuda", "assets/flags/AG.png");
  static const SA = CountryModel._("SA", "SAU", [966], "Arabie saoudite", "assets/flags/SA.png");
  static const AR = CountryModel._("AR", "ARG", [54], "Argentine", "assets/flags/AR.png");
  static const AM = CountryModel._("AM", "ARM", [374], "Arménie", "assets/flags/AM.png");
  static const AU = CountryModel._("AU", "AUS", [61], "Australie", "assets/flags/AU.png");
  static const AT = CountryModel._("AT", "AUT", [43], "Autriche", "assets/flags/AT.png");
  static const AZ = CountryModel._("AZ", "AZE", [994], "Azerbaïdjan", "assets/flags/AM.png");

  static const BS = CountryModel._("BS", "BHS", [1242], "Bahamas", "assets/flags/BS.png");
  static const BH = CountryModel._("BH", "BHR", [973], "Bahreïn", "assets/flags/BH.png");
  static const BD = CountryModel._("BD", "BGD", [880], "Bangladesh", "assets/flags/BD.png");
  static const BB = CountryModel._("BB", "BRB", [1246], "Barbade", "assets/flags/BB.png");
  static const BE = CountryModel._("BE", "BEL", [32], "Belgique", "assets/flags/BE.png");
  static const BZ = CountryModel._("BZ", "BLZ", [501], "Belize", "assets/flags/BZ.png");
  static const BJ = CountryModel._("BJ", "BEN", [229], "Bénin", "assets/flags/BJ.png");
  static const BT = CountryModel._("BT", "BTN", [975], "Bhoutan", "assets/flags/BT.png");
  static const BY = CountryModel._("BY", "BLR", [375], "Biélorussie", "assets/flags/BY.png"); // Bélarus
  static const BO = CountryModel._("BO", "BOL", [591], "Bolivie", "assets/flags/BO.png");
  static const BA = CountryModel._("BA", "BIH", [387], "Bosnie-Herzégonive", "assets/flags/BA.png");

  static const BW = CountryModel._("BW", "BWA", [267], "Botswana", "assets/flags/BW.png");
  static const BR = CountryModel._("BR", "BRA", [55], "Brésil", "assets/flags/BR.png");
  static const BN = CountryModel._("BN", "BRN", [673], "Brunei", "assets/flags/BO.png");
  static const BG = CountryModel._("BG", "BGR", [359], "Bulgarie", "assets/flags/BG.png");
  static const BF = CountryModel._("BF", "BFA", [226], "Burkina Faso", "assets/flags/BF.png");
  static const BI = CountryModel._("BI", "BDI", [257], "Burundi", "assets/flags/BO.png");

  static const CI = CountryModel._("CI", "CIV", [225], "Côte d'Ivoire", "assets/flags/CI.png");
  static const GA = CountryModel._("GA", "GAB", [241], "Gabon", "assets/flags/GA.png");

}
