
// import 'dart:async';
import 'dart:math' show max;

import 'package:country_utils/src/country_search_bloc.dart';
import 'package:flutter/material.dart';
import 'package:country_utils/src/country_model.dart';

Future<CountryModel?> showCountryPicker({
  required BuildContext context,
  String textFieldLabel: "Veuillez entrer le nom d'un pays",
  CountryModel? initialCountry,
}) {
  return showDialog<CountryModel>(
    context: context,
    builder: (_) => _CountryPickerDialog(
      textFieldLabel: textFieldLabel,
      initialCountry: initialCountry,
      onSelected: (country) {
        Navigator.of(context, rootNavigator: true).pop(country);
      },
    )
  );
}

class _CountryPickerDialog extends StatefulWidget {

  final String textFieldLabel;
  final void Function(CountryModel) onSelected;
  final CountryModel? initialCountry;

  _CountryPickerDialog({
    required this.textFieldLabel,
    required this.onSelected,
    this.initialCountry
  });

  @override
  __CountryPickerDialogState createState() => __CountryPickerDialogState();
}

class __CountryPickerDialogState extends State<_CountryPickerDialog> {

  final CountrySearchBloc _bloc = CountrySearchBloc(countries: CountryModel.values);

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    void _close() {
      Navigator.of(context, rootNavigator: true).pop(null);
    }

    return Dialog(
      child: Container(
        width: max(size.width / 2.40, 520.0),
        height: size.height * 0.65,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Padding(
              padding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0, bottom: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  const Text(
                    "Selectionner un pays",
                    style: TextStyle(
                      fontSize: 19.0,
                      fontWeight: FontWeight.w600
                    ),
                  ),

                  IconButton(
                    onPressed: _close,
                    splashRadius: 30.0,
                    padding: EdgeInsets.zero,
                    icon: Container(
                      width: 30.0,
                      height: 30.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.black12,
                        shape: BoxShape.circle
                      ),
                      child: const Icon(Icons.close_rounded)
                    )
                  ),
                ],
              ),
            ),

            _MyTextField(
              label: widget.textFieldLabel,
              onChanged: _bloc.search
            ),

            const Divider(height: 1.0),

            Expanded(
              child: StreamBuilder<List<CountryModel>>(
                initialData: _bloc.countries,
                stream: _bloc.listen,
                builder: (context, snapshot) {
                  final List<CountryModel> countries = snapshot.data!;
                  return ListView.builder(
                    itemCount: countries.length ,
                    itemBuilder: (_, index) => _CountryItemWidget(
                      country: countries[index],
                      onTap: widget.onSelected,
                    ),
                  );
                }
              )
            ),

            const SizedBox(height: 12.0)
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

}

/// La nature de l'information aditionel qu'on doit afficher
enum CountryWidgetType {
  iso_code, // For Burkina Faso: BF
  long_iso_code, // For Burkina Faso: BFA
  code // For Burkina Faso: 226
}

class _CountryItemWidget extends StatelessWidget {

  final void Function(CountryModel) onTap;
  final CountryModel country;
  final CountryWidgetType? type;

  const _CountryItemWidget({
    required this.onTap,
    required this.country,
    this.type
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(country),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
        child: Row(
          children: [

            //const SizedBox(width: 15.0),

            Image.asset(country.imageName, height: 20.0, package: "country_utils"),
            const SizedBox(width: 10.0),
            Text(country.name),
            Expanded(child: const SizedBox()),

            // if selection mettre un fond leger et une color une icon checked box bleu

            SizedBox(
              width: 26.0,
              child: Text(
                country.isoCode,
                style: const TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w600
                ),
              ),
            ),

            //const SizedBox(width: 15.0),

          ],
        ),
      ),
    );
  }
}

class _MyTextField extends StatefulWidget {

  final String label;
  final void Function(String) onChanged;

  _MyTextField({
    required this.label,
    required this.onChanged
  });

  @override
  __MyTextFieldState createState() => __MyTextFieldState();
}

class __MyTextFieldState extends State<_MyTextField> {

  final TextEditingController _controller = TextEditingController();

  void _clear() {
    _controller.clear();
    setState(() {});
    widget.onChanged("");
  }

  @override
  Widget build(BuildContext context) {

    final String value = _controller.value.text;

    return TextField(
      controller: _controller,
      onChanged: (newValue) {
        setState(() {});
        widget.onChanged(newValue);
      },
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: widget.label,
        prefixIcon: const Icon(Icons.search, color: Colors.black38),
        suffix: value.isEmpty
          ? null
          : _clearIcon()
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _clearIcon() {
    return Padding(
      padding: const EdgeInsets.only(right: 15.0),
      child: InkResponse(
        onTap: _clear,
        child: Container(
          width: 20.0,
          height: 20.0,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            color: Color(0x66C3C3C3),
            shape: BoxShape.circle
          ),
          child: const Icon(Icons.close, size: 12.0, color: Colors.blue)
        )
      ),
    );
  }

}
