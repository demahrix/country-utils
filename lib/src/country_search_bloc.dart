import 'dart:async';
import 'package:country_utils/country_utils.dart';

class CountrySearchBloc {

  final List<CountryModel> countries;
  // _searchValue; // voir si utile de stocker
  final StreamController<List<CountryModel>> _controller = StreamController();

  CountrySearchBloc({ required this.countries, String initialValue: "" }) {
    search(initialValue);
  }

  Stream<List<CountryModel>> get listen => _controller.stream;

  void search(String text) {
    // FIXME ignorer les lettres accentues
    text = text.toLowerCase();

    if (text == "") {
      _controller.sink.add(countries);
      return;
    }

    final List<CountryModel> result = [];

    final bool checkIsoCode = text.length < 4;
    for (int i=0, length=countries.length; i<length; ++i) {
      final country = countries[i];
      if (country.name.toLowerCase().contains(text)
        || (checkIsoCode && (country.isoCode.startsWith(text) || country.longIsoCode.startsWith(text)))
        ) {
        result.add(country);
      }
    }

    _controller.sink.add(result);

  }

  void dispose() {
    _controller.close();
  }

}
