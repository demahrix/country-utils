import 'package:country_utils/country_utils.dart';
import 'package:flutter/material.dart';

class CountryDataWidget extends StatelessWidget {

  final CountryModel? data;
  final Color? backgroundColor;
  final BorderRadiusGeometry borderRadius;
  final EdgeInsets padding;
  final VoidCallback? onTap;
  
  const CountryDataWidget({
    this.data,
    this.backgroundColor,
    this.borderRadius = const BorderRadius.all(Radius.circular(4.0)),
    this.padding = const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
    this.onTap
  });

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          padding: padding,
          decoration: BoxDecoration(
            color: backgroundColor ?? Color(0xFF66C3C3C3),
            borderRadius: borderRadius
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: data == null ? _emptyContent() : _content(),
          ),
        ),
      ),
    );
  }

  List<Widget> _content() {
    return [
      Image.asset(data!.imageName, height: 20.0, package: "country_utils"),
      const SizedBox(width: 8.0),
      Text(
        data!.name
      )
    ];
  }

  List<Widget> _emptyContent() {
    return [
      Text("Veuillez selectionner un pays"),
      const SizedBox(width: 10.0),
      Icon(Icons.arrow_drop_down_sharp)
    ];
  }


}
